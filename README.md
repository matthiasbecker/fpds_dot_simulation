# **FPDS_dot Simulation** #

Graphical simulation tool to visualize the two proposed algorithms proposed to enable a FPDS_dot schedule in an OSEK/AUTOSAR compliant operating system.
A executable version is located in the folder **executable**.

## **Configuration of the Simulator:** ##
The file "config.json" contains the basic configuration for the simulation tool. 
Here the user can decide whether a default task should be displayed or if the user should select a task using the dialog window. 
The user can also set the path to the default task description file.

## Task description file: ##
A task description file is used to describe the task in terms of the priority and preemption points. We use the JSON format since it is easily readable by humans. 

An example task might look as follows:

```
{
   “Priority”:3.0,
   “PreemptionPoints”:[
      {“pp”:4.0},
      {“pp”:5.0} 
   ]
}
```



The example describes a task with priority 3 and two preemption points “pp” with threshold 4 and 5 respectively. 

## Run the simulation: ##
Once the simulation is started it displays the access to the resources in a table at the bottom and additionally visualizes the tasks priority in a graph. At the right side some measurements are displayed. A user can press the keys ‘1’ and ‘2’ to switch between the two proposed algorithms, where ‘1’ is equivalent to “yield()” and ‘2’ to “yield*()”. 

## Exporting Figures: ##
Additionally a user can export the graph and table in the TpX format which is an easy to use TeX drawing tool, to generate vector graphics ([http://tpx.sourceforge.net/](http://tpx.sourceforge.net/)).
By pressing ‘p’ a dialog opens and the user has to specify a file name used for the exported graphic.