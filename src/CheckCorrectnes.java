import java.util.ArrayList;
import java.util.Stack;


/**
 * This class is used to check the correctness of the obtained schedule.
 * @author mbr04
 *
 */
public class CheckCorrectnes {

	/**
	 * Function to check the correctness of a schedule. We check both, the proper nesting as well as the correctness.
	 * @param pp				preemption points
	 * @param lockingPlan		locking plan as output of the init algorithms
	 * @param priorityLevels	number of priority levels used
	 * @return	0 if all is OK, -1 if one destination point was not reached, -2 if nesting is not proper
	 */
	public static int check(ArrayList<Integer> pp, int lockingPlan[][][], int priorityLevels){
		int retval = 0;
		int priority = pp.get(0);	//the initial priority is the first preemption point
		
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(priority);
		
		ArrayList<Integer> history = new ArrayList<Integer>();
		
		int schedRes = getMax(pp) + 1;
		
		for (int a = 0; a < pp.size(); a++) {
			
			boolean reachedPrio = false;
			boolean reachedUnlocking = false;
			
			for(int index = 0; index < ((priorityLevels) * 2); index++){
				if(stack.lastElement() == pp.get(a)){
					reachedPrio = true;
//					System.out.println("Reached Priority " + stack.lastElement());
					history.add(stack.lastElement());
//					System.out.println("Add to history " + stack.lastElement());
				}
				
				/****Increase the priority****/
				if(lockingPlan[a][index][1] == 1){	//add to the stack
					
					stack.push(lockingPlan[a][index][0]);	//add the resource to the stack
//					System.out.println("Add: " + stack.lastElement() + " PP: " + a);
					if(stack.lastElement() == pp.get(a)){
						reachedPrio = true;
//						System.out.println("Reached Priority " + stack.lastElement());
						//if(stack.lastElement() != schedRes){
							history.add(stack.lastElement());
//							System.out.println("Add to history " + stack.lastElement());
						//}
					}
					
				/****Decrease the Priority****/
				}else if(lockingPlan[a][index][1] == -1){	//remove from the stack
					
					if(stack.lastElement() == lockingPlan[a][index][0]){
						
//						System.out.println("Rem: " + stack.lastElement() + " PP: " + a);
						if(stack.lastElement() == pp.get(a)){
							if(reachedPrio == true)	reachedUnlocking = true;
							//if(stack.lastElement() != schedRes){
								history.add(stack.lastElement());
//								System.out.println("Add to history " + stack.lastElement());
							//}
							reachedPrio = true;
//							System.out.println("Reached Priority " + stack.lastElement());
						}
						
						int val = stack.pop();
						if(val != schedRes){
							if(history.contains(val)){
								history.remove(history.indexOf(val));
							}else{
//								System.err.println("Additional Locking.. Point:" + a);
								return -4;
							}
						}
					}
					else{
//						System.err.println("Impropper Nesting..");
						return -2;
					}
				}
			}
			if(reachedPrio == false){
//				System.err.println("Did not reach destination priority!");
				return -1;
			}
			if(reachedUnlocking == true){
				return -3;
			}
		}
		
		return retval;
	}
	
	private static int getMax(ArrayList<Integer> pp){
		int retval = 0;
		
		for (Integer integer : pp) {
			if(integer > retval) retval = integer;
		}
		
		return retval;
	}
}
