import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class Config {
	
	private static Config instance = null;
	
	private static String defaultPath = null;
	private static boolean choosFile = true;
	
	private Config() {
		parse();
	}
	
	public static Config getInst(){
		if(instance == null){
			instance = new Config();
		}
		return instance;
	}
	
	private static void parse(){
		JSONParser parser = new JSONParser();
		
		try{
			Object obj = parser.parse(new FileReader("config.json"));
			 
			JSONObject jsonObject = (JSONObject) obj;
			
			defaultPath = (String)jsonObject.get("Default_Path");
			
			choosFile = (Boolean)jsonObject.get("Select_File_At_Startup");

		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
	}
	
	public String getDefaultPath(){
		return defaultPath;
	}
	
	public boolean getChooseFile(){
		return choosFile;
	}
}
