import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import FPDS_3.FPDS_3;
import FPDS_3.OutputPlan;
import FPDS_3.Resource;


public class FPDSGraph extends Component{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	final static int RESOURCE_LOCKED = 1;
	final static int RESOURCE_UNLOCKED = -1;
	final static int RESOURCE_NOT_USED = 0;
	
	private static final int PP_DISTANCE_X = 80;
	private static final int PP_DISTANCE_Y = 50;
	private static final int BORDER_LEFT = 70;
	private static final int TEXTBOX_WIDTH = 160;
	
	private int PP_LOCKED_DISTANCE = 5;
	private File configFile = null;
	private int tbWidth = 0;
	
	private int fptsVersion = 4;	//Start version, we draw the worse first so we have the largest 
									//amount of resource calls for the size of the table
	
	private int width = 0;
	private int height = 0;
	private int basicHeight = 0;
	private int basicWidth = 0;
	
	public FPDSGraph(File configFile) {
		tbWidth = TEXTBOX_WIDTH;
		
		this.configFile = configFile;
	}
	
	public void paint(Graphics g){
		ArrayList<Integer> pp = JSONReader.getModPreemptionPoints(configFile);
		
		Resource.resetCounter();	//reset the global counter for lockings and unlockings 
		
		FPDS_3 fpts3 = new FPDS_3(pp, fptsVersion);	//create a new "locking algorithm"
		
		int levels = getPriorityLevels(pp); //we need to add the scheduler
		int points = pp.size();
		
		basicWidth = (points - 1) * PP_DISTANCE_X + BORDER_LEFT + 50 + TEXTBOX_WIDTH;
		basicHeight = levels * PP_DISTANCE_Y + 20;
		
		height = basicHeight;
		width = basicWidth;
		
		
		drawResources(g, pp, fpts3);
		
		//draw the resources
		drawBasics(g, pp);	//Draw all the lines, preemption points and label the axes
		
		drawTextbox(g, fpts3);
		
		drawPrimitiveCalls(g, fpts3, pp);	//list all calls to AUTOSAR/OSEK primitives
		
		JFrame frame = (JFrame) SwingUtilities.getRoot(this);	//get the frame we are embedded in
		frame.setSize(width, height);
	}
	
	private void drawBasics(Graphics g, ArrayList<Integer> pp){
		Graphics2D g2 = (Graphics2D) g;
        Dimension size = getSize();
        Composite origComposite;
        
        Font font = new Font("sansserif", Font.PLAIN, 18);
        Font fontSmall = new Font("sansserif", Font.BOLD, 12);
        g.setFont(font);
        
        g.setColor(Color.BLACK);
//        System.out.println("Priority max: " + getMax(pp));
//        System.out.println("Priority min: " + getMin(pp));
//        System.out.println("Priority levels: " + getPriorityLevels(pp));
        
        //Draw the horizontal lines
        for(int i = 0; i < getPriorityLevels(pp); i++){
        	g2.drawLine(BORDER_LEFT - 10, 25 + (PP_DISTANCE_Y * i), BORDER_LEFT + 25 + PP_DISTANCE_X * (pp.size()-1), 25 + (PP_DISTANCE_Y * i));
        }
        
        //Draw the vertical lines
        for(int i = 0; i < pp.size(); i++){
        	g2.drawLine(BORDER_LEFT + (i * PP_DISTANCE_X), 10, BORDER_LEFT + (i * PP_DISTANCE_X), 35 + ((getPriorityLevels(pp) - 1) * PP_DISTANCE_Y));
        }
        
        //Draw the points
        
        int maxPoints = getPriorityLevels(pp);
        
        for(int i = 0; i < pp.size(); i++){
        	
        	int r = 5;	//radius
        	int x = BORDER_LEFT + (PP_DISTANCE_X * i) - 5;	//x coordinate
        	int y =  (maxPoints - (pp.get(i) - getMin(pp)+1)) * PP_DISTANCE_Y + 25 - 5;	//y coordinate
        	
        	g2.fillOval(x, y, r * 2, r * 2);
        }
        
        int yFontDistanceLeft = 10;
        
        //Label the y axis
        g2.drawString("\u03c4", yFontDistanceLeft, 25 + 6);
        g.setFont(fontSmall);
        g2.drawString("sched", yFontDistanceLeft + 9, 25 + 5 + 6);
        
        for(int i = 0; i < (getPriorityLevels(pp) - 1); i++){
        	g.setFont(font);
        	g2.drawString("\u03c4", yFontDistanceLeft, 22 + 6  + (PP_DISTANCE_Y * i) + 50);
        	g.setFont(fontSmall);
        	g2.drawString(Integer.toString(getMax(pp) - i), yFontDistanceLeft + 9, 22 + 11  + (PP_DISTANCE_Y * i) + 50);
        }
        
        g.setFont(font);
        
        //Label the x axis
        g2.drawString("\u0398", BORDER_LEFT - 20, (int) (basicHeight - 13));
        g.setFont(fontSmall);
        g2.drawString("Start", BORDER_LEFT - 5, (int) (basicHeight - 10));
        g.setFont(font);
        g2.drawString("\u0398", BORDER_LEFT - 20 + (PP_DISTANCE_X * (pp.size()-1)), (int) (basicHeight - 13));
        g.setFont(fontSmall);
        g2.drawString("Stop", BORDER_LEFT - 5 + (PP_DISTANCE_X * (pp.size()-1)), (int) (basicHeight - 10));
        
        for(int i = 0; i < pp.size() - 2; i++){
        	g.setFont(font);
        	g2.drawString("\u0398", BORDER_LEFT - 10 + (PP_DISTANCE_X * (i + 1)), (int) (basicHeight - 13));
        	g.setFont(fontSmall);
        	g2.drawString(Integer.toString(i + 1), BORDER_LEFT + 5 + (PP_DISTANCE_X * (i + 1)), (int) (basicHeight - 10));
        }
	}
	
    /**
     * Function to get the max priority level of this tasks preemption points
     * @param pp
     * @return
     */
    private int getMax(ArrayList<Integer> pp){
    	int max = 0;
    	
    	for (Integer value : pp) {
			if(value > max) max = value;
		}
    	
    	return max;
    }
    
    /**
     * Function to get the min priority level of this preemption point (to make sure they are all larger or equal to the priority)
     * @param pp
     * @return
     */
    private int getMin(ArrayList<Integer> pp){
    	int min = getMax(pp);
    	
    	for (Integer value : pp) {
			if(value < min) min = value;
		}
    	return min;
    }
    
    /**
     * Get the number of used priority levels (max-min)
     * @param pp
     * @return
     */
    private int getPriorityLevels(ArrayList<Integer> pp){
    	int levels = getMax(pp) - getMin(pp);
    	
    	if(getMin(pp) < pp.get(0)) System.err.println("Preemption point less than priority!");
    	
    	return levels + 2;
    }
    
    private void drawResources(Graphics g, ArrayList<Integer> preemptionPoints, FPDS_3 fpts3){
    	
		
    	int maxPoints = getPriorityLevels(preemptionPoints);
    	
    	Graphics2D g2 = (Graphics2D) g;
        Dimension size = getSize();
        Composite origComposite;
    	
		int plan[][][] = fpts3.getResourceLockings();
		
		int correct = CheckCorrectnes.check(preemptionPoints, plan, maxPoints);
		if(correct != 0){
			if(correct == -1){
				System.out.println("*************Destination point not reached!*************");
			}else if(correct == -2){
				System.out.println("********************Improper nesting!*******************");
			}else if(correct == -3){
				System.out.println("********************Reached Locking!********************");
			}else if(correct == -4){
				System.out.println("**********************Non Optimal!**********************");
			}
		}
		
		int unlockPoint = 0;
		int pointID = 0;
		
		g.setColor(Color.GRAY);
		int lockedDistance = 0;
		
		for(int point = 0; point < preemptionPoints.size(); point++){	//draw for all points
			for(int action = 0; action < (getPriorityLevels(preemptionPoints) * 2); action++){
				
				if(plan[point][action][1] == RESOURCE_NOT_USED) break;
				else if(plan[point][action][1] == RESOURCE_LOCKED){
					unlockPoint = searchUnlock(plan, point, preemptionPoints, plan[point][action][0]);
					pointID = plan[point][action][0];
//					System.out.println("id: " + plan[point][action][0]+ " locked: " + point + " unlocked at: " + unlockPoint);
					
					int unlockDistance = getUnlockDistance(pointID, plan, unlockPoint, preemptionPoints);
					
					//Draw the rectangle visualizing the locked area
					g2.fillRect(BORDER_LEFT + (point * PP_DISTANCE_X) + (lockedDistance * PP_LOCKED_DISTANCE) + PP_LOCKED_DISTANCE, 
							(maxPoints - (pointID - getMin(preemptionPoints)+1)) * PP_DISTANCE_Y + 25, 
								PP_DISTANCE_X * (unlockPoint - point) - (lockedDistance * PP_LOCKED_DISTANCE) - (unlockDistance * PP_LOCKED_DISTANCE) - (2 * PP_LOCKED_DISTANCE), 
								((pointID - getMin(preemptionPoints) )) * PP_DISTANCE_Y);
					lockedDistance++;
				}
			}
			lockedDistance = 0;
		}
    }
    
    
    private int getUnlockDistance(int id, int[][][] plan, int unlockPoint, ArrayList<Integer> pp){
    	
    	int retval = 0;
    	
    	for(int i = 0; i < getPriorityLevels(pp); i++){
    		if(plan[unlockPoint][i][1] == RESOURCE_UNLOCKED){
    			if(plan[unlockPoint][i][0] < id) retval++;
    		}
    	}
    	
    	
    	
    	return retval; 
    }
    
//    private int getDistance(ArrayList<Integer> pp, int id){
//    	for (int i = 0; i < pp.size(); i++) {
//			if(pp.get(i) == id){
//				System.out.println("Distance: " + i);
//				return i;
//			}
//		}
//    	return 0;
//    }
    
    private int searchUnlock(int[][][] outPlan, int lockingPP, ArrayList<Integer> pp, int id){
    	int unlockingPP = 0;
    	
    	for(int i = lockingPP+1; i < pp.size(); i++){
    		unlockingPP = i;	//in the worst case it's the last one
    		for(int k = 0; k < (getPriorityLevels(pp) * 2); k++){
    			if(outPlan[i][k][0] == id && outPlan[i][k][1] == -1){
    				return unlockingPP;
    			}
    		}
    	}
    	
    	return unlockingPP;
    }

    /**
     * This method is called from outside to repaint everything with a new FPDS dot algorithm
     * @param fptsVersion
     */
    public void update(int fptsVersion){
    	this.fptsVersion = fptsVersion;
    	Resource.resetCounter();
    	repaint();
    }
    
    /**
     * This method draws the textbox with information about the used algorithm, max lockings etc.
     * @param g
     * @param fpts3
     */
    private void drawTextbox(Graphics g, FPDS_3 fpts3){
		Graphics2D g2 = (Graphics2D) g;
        Dimension size = getSize();
        Composite origComposite;
        
        Font font = new Font("sansserif", Font.PLAIN, 18);
        Font fontSmall = new Font("sansserif", Font.BOLD, 12);
        g.setFont(font);
        
        g.setColor(Color.WHITE);
        g2.fillRect((width - tbWidth), 0, tbWidth,basicHeight);
        g.setColor(Color.LIGHT_GRAY);
        g2.drawLine((width - tbWidth), 0, (width - tbWidth), basicHeight);
        g.setColor(Color.GRAY);
        g2.drawLine((width - tbWidth)-1, 0, (width - tbWidth)-1, basicHeight);
        
        g.setColor(Color.BLACK);
        g2.drawString("Algorithm: ", (int)(size.getWidth() - tbWidth) + 15, 30);
        g2.drawString(Integer.toString(fptsVersion), (int)(size.getWidth() - tbWidth) + 125, 30);
        g2.drawString("Resources:", (int)(size.getWidth() - tbWidth) + 15, 30 + (1 * 25));
        g2.drawString(Integer.toString(fpts3.getNumberOfResources()), (int)(size.getWidth() - tbWidth) + 125, 30 + (1 * 25));
        g2.drawString("Lockings:", (int)(size.getWidth() - tbWidth) + 15, 30 + (2 * 25));
        g2.drawString(Integer.toString(fpts3.getNumberOfLockings()), (int)(size.getWidth() - tbWidth) + 125, 30 + (2 * 25));
        g2.drawString("Unockings:", (int)(size.getWidth() - tbWidth) + 15, 30 + (3 * 25));
        g2.drawString(Integer.toString(fpts3.getNumberOfUnlockings()), (int)(size.getWidth() - tbWidth) + 125, 30 + (3 * 25));
    }
    
    /**
     * This method is used to print all calls to the AUTOSAR/OSEK primitives in a table below the graph
     * @param g	Graphics object we use to draw on the panel
     * @param fpts3	The instance of the FPDS dot algorithm we use
     */
    private void drawPrimitiveCalls(Graphics g, FPDS_3 fpts3, ArrayList<Integer> pp){
    	
    	int[][][] calls = fpts3.getResourceLockings();	//returns the list with all lockings
    	int maxCalls = getMaxNumberOfLockingsPerPoint(fpts3, pp);
//    	System.out.println("Maximal number of calls: " + maxCalls);
    	
		//JFrame frame = (JFrame) SwingUtilities.getRoot(this);	//get the frame we are embedded in
		//frame.setSize(width, height + (maxCalls * 30));	//change the frame size
		int rowHeight = 30;
		height = basicHeight + (maxCalls * rowHeight);

		Graphics2D g2 = (Graphics2D) g;
        Dimension size = getSize();
        Composite origComposite;
        
        Font font = new Font("sansserif", Font.PLAIN, 18);
        Font fontSmall = new Font("sansserif", Font.BOLD, 12);
        Font fontSuperSmall = new Font("sansserif", Font.BOLD, 12);
        g.setFont(fontSuperSmall);
        
        g.setColor(Color.WHITE);
        g2.fillRect(0, basicHeight, width,height - basicHeight);
        g.setColor(Color.GRAY);
        g2.drawLine(0, basicHeight, width - tbWidth, basicHeight);
        g.setColor(Color.LIGHT_GRAY);
        g2.drawLine(0, basicHeight + 1, width - tbWidth, basicHeight + 1);
        
        g.setColor(Color.LIGHT_GRAY);
        g2.drawLine((width - tbWidth), basicHeight, (width - tbWidth), height);
        g.setColor(Color.GRAY);
        g2.drawLine((width - tbWidth) - 1,basicHeight, (width - tbWidth) - 1, height);
        
        g.setColor(Color.LIGHT_GRAY);
        
        //draw the horizontal lines
        for(int i = 0; i < (pp.size()); i++){	//we draw one line between each PP
        	g2.drawLine(BORDER_LEFT - (PP_DISTANCE_X / 2) + (PP_DISTANCE_X * i), basicHeight, BORDER_LEFT - (PP_DISTANCE_X / 2) + (PP_DISTANCE_X * i), height);
        }
        
        //draw the vertical lines 
        for(int i = 0; i < maxCalls - 2; i++){
        	g2.drawLine(0, basicHeight + ((i + 1) * rowHeight), width - tbWidth, basicHeight + ((i + 1) * rowHeight));
        	g2.drawString(Integer.toString(i + 1), 10, basicHeight + ((i + 1) * rowHeight) - 9);
        }
        
        //...and add line numbers
        g.setColor(Color.black);
        for(int i = 0; i < maxCalls - 1; i++){
        	g2.drawString(Integer.toString(i + 1), 10, basicHeight + ((i + 1) * rowHeight) - 9);
        }
        
        //add the text to the cells
        String label = null;
        g.setColor(Color.BLACK);
        
        for(int point = 0; point < pp.size(); point++){
        	for(int call = 0; call < maxCalls; call++){
        		if(calls[point][call][1] == 0) break;	//stop if there is no locking command in this cell!
        		int resource = calls[point][call][0];
        		
        		//get the command and build the string
        		if(calls[point][call][1] == 1){	//call to lock the resource
        			label = new String("Get(" + resource + ")");
        			if(resource == getMax(pp) + 1) label = "Get(SCHED)";
        		}else if(calls[point][call][1] == -1){	//call to unlock the resource
        			label = new String("Rel(" + resource + ")");
        			if(resource == getMax(pp) + 1) label = "Rel(SCHED)";
        		}
        		g2.drawString(label, BORDER_LEFT - (PP_DISTANCE_X / 2) + (PP_DISTANCE_X * point) + 8, basicHeight + ((call + 1) * rowHeight) - 9);
        	}
        }
    }
    	
    /**
     * This function returns the maximum number of calls to AUTOSAR/OSEK primitives of one preemption point
     * @param fpts3 The instance of the FPDS dot algorithm we use
     * @param pp	The preemption point configuration of the task
     * @return		Maximum number of calls to the AUTOSAR/OSEK primitive of any of the preemption points
     */
    private int getMaxNumberOfLockingsPerPoint(FPDS_3 fpts3, ArrayList<Integer> pp){
    	int retval = 0;
    	
    	int[][][] calls = fpts3.getResourceLockings();	//array with all information about locking and unlocking of resources
    	int numberOfPP = pp.size();						//number of preemption points
    	int tmpCount = 0;
    	
    	for(int i = 0; i < numberOfPP; i++){
    		for(int k = 0; k < (fpts3.getNumberOfResources() * 2); k++){
    			if(calls[i][k][1] == 0) break;	//check if this entry is not used
    			tmpCount++;
    		}
    		if(tmpCount > retval) retval = tmpCount;
    		tmpCount = 0;
    	}
    	
    	return retval;
    }
}
