package FPDS_3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JOptionPane;

public class FPDS_3 {
	
	private static final int FPTS_1 = 1;	//The second version, locking all resources on the way
	private static final int FPTS_2 = 2;	//The third version, locking only needed resources
	private static final int FPTS_3 = 3;	//The fourth version, the one with the minimas (not correct!!!)
	private static final int FPTS_4 = 4;
	
	private int FPTS_VERSION = FPTS_2;
	
	private ArrayList<Resource> resources = null;
	private boolean states[] = null;
	private int index = 1;
	private int currentPrio = 0;
	private ArrayList<Integer> pp = null;
	private OutputPlan outPlan = null;
	
	public FPDS_3(ArrayList<Integer> pp, int version) {
		
		FPTS_VERSION = version;
		
		this.pp = pp;
		outPlan = new OutputPlan(pp.size(), getPriorityLevels());
		
		//create resources for all levels
		resources = new ArrayList<Resource>();
		
		for (Integer level : pp) {	//add resources to the list
			Resource tmp = new Resource(level);
			
			boolean doubleResource = false;
			
			//if(tmp.getCeiling() != pp.get(0)){	//don't add a resource with the tasks priority
				for (Resource r : resources) {
					if(r.getCeiling() == tmp.getCeiling()){	//make sure not to add two resources with the same ceiling
						doubleResource = true;		
					}
				}
				if(doubleResource == false) resources.add(tmp);
				doubleResource = false;
		//	}
		}
		
		Collections.sort(resources);	//sort the resources
		
		//add the scheduler as resource
		resources.add(new Resource(resources.get(resources.size()-1).getCeiling() + 1));
		
		Collections.sort(resources);	//sort the resources again
		
		resources.get(0).lock();	//lock the resource according to the task prio (so the task has its priority)
		
		states = new boolean[resources.size()];	//create the array with states of the resources
		
		for(int i = 0; i < resources.size(); i++){
			states[i] = false;	//unlocked resource
		}
		
		states[0] = true;
		
		for (int i = 0; i < resources.size(); i++) {
			System.out.println("Ceiling: " + resources.get(i).getCeiling() + " and has the state " + states[i]);
		}
		
		runSimulation();
	}
	
	public int[][][] getResourceLockings(){
		return outPlan.getPlan();
	}
	
    /**
     * Function to get the max priority level of this tasks preemption points
     * @param pp
     * @return
     */
    private int getMax(){
    	int max = 0;
    	
    	for (Integer value : pp) {
			if(value > max) max = value;
		}
    	
    	return max;
    }
    
    /**
     * Function to get the min priority level of this preemption point (to make sure they are all larger or equal to the priority)
     * @param pp
     * @return
     */
    private int getMin(){
    	int min = getMax();
    	
    	for (Integer value : pp) {
			if(value < min) min = value;
		}
    	return min;
    }
    
    /**
     * Get the number of used priority levels (max-min)
     * @param pp
     * @return
     */
    private int getPriorityLevels(){
    	int levels = getMax() - getMin();
    	
    	if(getMin() < pp.get(0)) System.err.println("Preemption point less than priority!");
    	
    	return levels + 2;
    }
    
    /**
     * This function is used by the algorithm to increase the priority level. The parameter decides if the
     * resource is locked or not.
     * @param lock
     */
    private void increasePriority(boolean lock){
    	if(lock == true){
    		if(!resources.get(index).lock()) System.err.println("Resource " + resources.get(index - 1).getCeiling() + " already locked!");
    		outPlan.lockResource(resources.get(index).getCeiling());
    		states[index] = true;
    	}
    	if(index < resources.size() - 1){
    		index++;
    	}
    	currentPrio = resources.get(index - 1).getCeiling();
    }
    
    /**
     * This function is used by the algorithm to decrease the priority level. 
     */
    private void decreasePriority(){
    	if(states[index - 1] == true){	//the resource is locked
    		if(!resources.get(index - 1).unlock()) System.err.println("Resource " + resources.get(index - 1).getCeiling() + " is not locked!");
    		outPlan.unlockResource(resources.get(index - 1).getCeiling()); 
    		states[index - 1] = false;
    	}
    	if((index - 1) > 0){
    		index--;
    	}
    	currentPrio = resources.get(index - 1).getCeiling();
    }
    
    /**
     * Function used to lock the scheduler, e.g. lock the resource with the largest ceiling in the system
     */
    private void lockScheduler(){
    	if(!resources.get(resources.size() - 1).lock()) System.err.println("Scheduler is already locked!");
		outPlan.lockResource(resources.get(resources.size() - 1).getCeiling()); 
    }
    
    /**
     * Function is used to unlock the scheduler, e.g. the resource with the largest ceiling in the system
     */
    private void unlockScheduler(){
    	if(!resources.get(resources.size() - 1).unlock()) System.err.println("Scheduler is not locked!");
		outPlan.unlockResource(resources.get(resources.size() - 1).getCeiling());
    }
    
    /**
     * This function checks if a resource has to be locked now or not
     * @param point
     * @param ceiling
     * @param pp
     * @return
     */
    private boolean checkPriorityLevel(int point, int resourceCeiling){
    	boolean retval = false;
    	
    	switch (FPTS_VERSION) {
		case FPTS_1:
			return true;
		case FPTS_2:
			int nextIndex = point + 1;
    		while(pp.get(nextIndex) > pp.get(point)){
    			if(pp.get(nextIndex) == resourceCeiling) return true;
    			nextIndex++;
    		}
			break;
		default:
			break;
		}
    	    	
    	return retval;
    }
    
    /**
     * This function returns a subset of points which lie in the hill starting at preemption point a
     * @param a The index of the originating point of that hill
     * @return A list with all resources in the hill
     */
    private ArrayList<Integer> hill(int a){
    	ArrayList<Integer> retval = new ArrayList<Integer>();
    	
    	int nextIndex = a + 1;
		while(pp.get(nextIndex) > pp.get(a)){	//add the point to the hill as long as the priority is greater than the initial priority of the hill
			retval.add(pp.get(nextIndex));	//add the point to the list
			nextIndex++;	//go to the next point
		}
    	
//		System.out.println("Hill:");
//		for (Integer integer : retval) {
//			System.out.println("Value: " + integer);
//		}
    	return retval;
    }
    
    /**
     * This function is used to identify the local minimas of a hill
     * @param hill
     * @return returns a set of all local minimas within the hill
     */
   private  ArrayList<Integer> localMinima(ArrayList<Integer> hill){
    	ArrayList<Integer> retval = new ArrayList<Integer>();
    	
    	int point = 0;
    	int index = 1;	//we start with the second point!!!
    	
    	if(hill.size() > 1){	//if there is only one point there is no local minima
    		while(index < hill.size() - 1){
	    		point = hill.get(index);	
	    		if(point < hill.get(index -1) && (point < hill.get(index + 1))){
	    			retval.add(hill.get(index));
	    		}
	    		index++;
    		}
    	}   	
    	
//    	System.out.println("Local Minima:");
//    	for (Integer integer : retval) {
//			System.out.println("Value: " + integer);
//		}
    	return retval;
    }
    
   /**
    * This function returns the deepest minimas till the deepest one
    * @param lm
    * @return
    */
   private  ArrayList<Integer> getSublistTillDeepestMinima(ArrayList<Integer> lm){
	   ArrayList<Integer> retval = new ArrayList<Integer>();
	   if(lm.size() != 0){
			int deepestMinima = min(lm);
			
			int index = 0;
			while(lm.get(index) != deepestMinima){
				retval.add(lm.get(index));
				index++;
			}
			 retval.add(deepestMinima);
	   }
	  
	   Collections.sort(retval);
	   
//	   System.out.println("Part Minimas:");
//   	for (Integer integer : retval) {
//			System.out.println("Value: " + integer);
//		}
	   return retval;
   }
   
    private void yield(int prev, int now, int next, int currentIndex){
    	if(prev != -1){
    		unlockScheduler();
    		if(prev > now){	//previous PP had higher ceiling than the current one
    			do{
    				decreasePriority();
    			}while(currentPrio > now);
    		}
    	}
    	if(next != -1){
    		if(FPTS_VERSION == FPTS_1 || FPTS_VERSION == FPTS_2){
	    		if(next > now){ //next PP has higher ceiling than the current one 
	    			do{
	    				boolean lock = checkPriorityLevel(currentIndex, resources.get(index).getCeiling());//checkPriorityLevel(point, ceiling, pp);
	    				increasePriority(lock);
	    			}while(currentPrio < next);
	    		}
    		}else if(FPTS_VERSION == FPTS_3){
    			if(next > now){
    				ArrayList<Integer> hill = hill(currentIndex);	//get the hill
    				ArrayList<Integer> lm = localMinima(hill);		//get the local minimas
    				ArrayList<Integer> tillDeepestMinima = getSublistTillDeepestMinima(lm);	//get the list of priority values till the deepest point
    				
    				//lock all local minima on the way till the deepest one, they are all included in tillDeepestMinima
    				//so we need to find the deepest local minima first!
    				int minimaIndex = 0;
    				if(tillDeepestMinima.size() != 0){
    				do{
    					boolean lock = false;
    					
    					if(minimaIndex < tillDeepestMinima.size()){
	    					if(resources.get(index).getCeiling() == tillDeepestMinima.get(minimaIndex)){
	    						lock = true;
	    						minimaIndex++;
	    					}
    					}
    					increasePriority(lock);
    				}while(currentPrio < next - 1);
    				}
    				
    				//lock the next point
    				//we first need to push the resources we do not need to lock! and then lock the final one
    				do{
	    				if(resources.get(index).getCeiling() < next) increasePriority(false);
	    				else increasePriority(true);
	    			}while(currentPrio < next);
    			}
    		}else if(FPTS_VERSION == FPTS_4){
    			/* This is the fourth version of the algorithm. In order to lock only the necessary resources we first need to 
    			 * get the hill() which gives us all points within the hill. Now we start looking at the points starting from lowest priority.
    			 * If there is an other point with lower priority and index smaller than this point we don't lock it. otherwise we lock it.
    			 * */
    			if(next > now){
	    			ArrayList<Integer> hill = hill(currentIndex);	//get the hill
	    			
	    			do{
	    				int prio = resources.get(index).getCeiling();
	    				if(hill.contains(prio) == true){
	    					//the next priority is part of the hill
	    					
	    					//find first occurrence of the next priority
	    					int firstOccurrence = getFirstIndex(hill, prio);
	    					
	    					//check if there is a lower priority earlier in the hill
	    					boolean lock = true;
	    					for(int i = 0; i < firstOccurrence; i++){
	    						if(hill.get(i) < prio){
	    							lock = false;
	    						}
	    					}
	    					increasePriority(lock);
	    					
	    				}else{
	    					increasePriority(false);  //the priority is not in the hill, so we don't need to lock it
	    				}
	    			}while(currentPrio < next);	//as long as we are lower than the 
    			}
    		}
    		lockScheduler();
    	}
    }
    
    /**
     * Function to get the minimal value out of the set of minimal in the input set
     * @param input
     * @return
     */
    private int min(ArrayList<Integer> input){
    	int minima = input.get(0);
    
    	for (Integer integer : input) {
			if(integer < minima) minima = integer;
		}
    	
    	return minima;
    }
    
    /**
     * This function returns the index of the first array element with the value 
     * @param  hill	array of integer
     * @param  value	value we are looking for
     * @return index of the first occurrence of the value in the array
     */
    private int getFirstIndex(ArrayList<Integer> hill, int value){
    	
    	for (int i = 0; i < hill.size(); i++) {
			if( hill.get(i) == value) return i;
		}
    	
    	return -1;
    }
    
    private void runSimulation(){
  	
    	int previous = -1;	//there is no previous point
    	int next = 1;		//the next point has index 1 at the beginning
    	int current = 0;
    	
    	//Resource.resetCounter();
    	currentPrio = resources.get(0).getCeiling();
    	
    	for(int currentIndex = 0; currentIndex < pp.size(); currentIndex++){
    		
    		System.out.println("Preemption point: " + currentIndex);
    		
    		if(currentIndex == 0) previous = -1;
    		else previous = pp.get(currentIndex - 1);
    		
    		current = pp.get(currentIndex);
    		
    		if(currentIndex == pp.size() - 1) next = -1;
    		else next = pp.get(currentIndex + 1);
    		
    		yield(previous, current, next, currentIndex);
    		
    		outPlan.nextPP();
    	}
    	
    	System.out.println("Resource lockings: " + (Resource.getNumberOfLockings() - 1));	//we don't count the task priority
    	System.out.println("Resource unlockings: " + Resource.getNumberOfUnlockings());    	
    }
    
    public int getNumberOfResources(){
    	return resources.size() - 2;	//we subtract the scheduler and the task itself!
    }

	public int getNumberOfLockings() {
		return Resource.getNumberOfLockings() - 1;	//we subtract the initial locking of the resource equal to the tasks priority
	}

	public int getNumberOfUnlockings() {
		return Resource.getNumberOfUnlockings();
	}
}
