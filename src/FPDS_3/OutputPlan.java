package FPDS_3;

import java.util.ArrayList;

public class OutputPlan {

	final static int RESOURCE_LOCKED = 1;
	final static int RESOURCE_UNLOCKED = -1;
	final static int RESOURCE_NOT_USED = 0;
	final static int ID = 0;
	final static int ACTION = 1;
	
	private int instructions[][][] = null;
	
	private int activePP = 0;
	private int index = 0;
	
	private int resourceNumber = 0;
	private int pointsNumber = 0;
	
	public OutputPlan(int numberOfPP, int numberOfResources) {
		
		instructions = new int[numberOfPP][numberOfResources * 2][2];	//the main list we use
		resourceNumber = numberOfResources;
		pointsNumber = numberOfPP;
	}
	
	public void nextPP(){
		//System.out.println(instructions.length);
		if(activePP < instructions.length){
			activePP++;
			index = 0;
		}
	}
	
	public void lockResource(int resourceID){
		instructions[activePP][index][ACTION] = RESOURCE_LOCKED;
		instructions[activePP][index][ID] = resourceID;
		index++;
	}
	
	public void unlockResource(int resourceID){
		instructions[activePP][index][ACTION] = RESOURCE_UNLOCKED;
		instructions[activePP][index][ID] = resourceID;
		index++;
	}
	
	public int[][][] getPlan(){
		return instructions;
	}
}
