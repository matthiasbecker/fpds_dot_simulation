package FPDS_3;

public class Resource implements Comparable<Resource>{
	
	//Those are for the whole class
	private static int lockings = 0;
	private static int unlockings = 0;
	
	private boolean state = false;
	private int ceiling = 0;
	
	public Resource(int ceilingPriority) {
		ceiling = ceilingPriority;
	}
	
	/**
	 * Return the state of the resource
	 * @return the state
	 */
	public boolean getState(){
		return state;
	}
	
	/**
	 * Function to return the ceiling of this resource
	 * @return
	 */
	public int getCeiling(){
		return ceiling;
	}
	
	/**
	 * Function to lock the resource
	 * @return true if successful and false otherwise
	 */
	public boolean lock(){
		if(state == false){
			state = true;
			lockings++;
			System.out.println("  Locked Resource: " + ceiling);
			return true;
		}else{
			return false;	//the resource is already locked
		}
	}
	
	/**
	 * Function to unlock the resource
	 * @return true if successful an false otherwise 
	 */
	public boolean unlock(){
		if(state == true){
			state = false;
			unlockings++;
			System.out.println("  Unlocked Resource: " + ceiling);
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Return the number of "lock resource" calls
	 * @return
	 */
	public static int getNumberOfLockings(){
		return lockings;
	}
	
	/**
	 * Return the number of "unlock resource" calls
	 * @return
	 */
	public static int getNumberOfUnlockings(){
		return unlockings;
	}

	@Override
	public int compareTo(Resource res2) {
		if(res2.getCeiling() < this.getCeiling()) return 1;
		else if(res2.getCeiling() > this.getCeiling()) return -1;
		else return 0;
	}
	
	/**
	 * Reset the counter for global lockings and unlockings of resources
	 */
	public static void resetCounter(){
		lockings = 0;
		unlockings = 0;
	}
}
