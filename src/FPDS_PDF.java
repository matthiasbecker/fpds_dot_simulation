import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import FPDS_3.FPDS_3;
import FPDS_3.Resource;
import TpX.TpX;


public class FPDS_PDF {

	final static int RESOURCE_LOCKED = 1;
	final static int RESOURCE_UNLOCKED = -1;
	final static int RESOURCE_NOT_USED = 0;
	
	private static final int PP_DISTANCE_X = 16;
	private static final int PP_DISTANCE_Y = 4;
	private static final int BORDER_LEFT = 10;
	private static final int TEXTBOX_WIDTH = 0;
	
	private int PP_LOCKED_DISTANCE = 1;
	private File configFile = null;
	private int tbWidth = 0;
	
	private int fptsVersion = 4;	//Start version, we draw the worse first so we have the largest 
									//amount of resource calls for the size of the table
	
	private int width = 0;
	private int height = 0;
	private int basicHeight = 0;
	private int basicWidth = 0;
	
	public FPDS_PDF(File configFile) {
		tbWidth = TEXTBOX_WIDTH;
		
		this.configFile = configFile;
	}
	
	public void generatePDF(String name){
		
		TpX g = new TpX(name, "Access to the pseudo-resources", "fig:screen");
		
		ArrayList<Integer> pp = JSONReader.getModPreemptionPoints(configFile);
		
		Resource.resetCounter();	//reset the global counter for lockings and unlockings 
		
		FPDS_3 fpts3 = new FPDS_3(pp, fptsVersion);	//create a new "locking algorithm"
		
		int levels = getPriorityLevels(pp); //we need to add the scheduler
		int points = pp.size();
		
		basicWidth = (points - 1) * PP_DISTANCE_X + BORDER_LEFT + 50 + TEXTBOX_WIDTH;
		basicHeight = levels * PP_DISTANCE_Y + 20;
		
		height = basicHeight;
		width = basicWidth;
		
		
		drawResources(g, pp, fpts3);
		
		//draw the resources
		drawBasics(g, pp);	//Draw all the lines, preemption points and label the axes
		
//		drawTextbox(g, fpts3);
		
		drawPrimitiveCalls(g, fpts3, pp);	//list all calls to AUTOSAR/OSEK primitives
		
//		JFrame frame = (JFrame) SwingUtilities.getRoot(this);	//get the frame we are embedded in
//		frame.setSize(width, height);
		
		g.closeFile();
	}
	
	private void drawBasics(TpX g, ArrayList<Integer> pp){
//        Dimension size = getSize();
        
        //Draw the horizontal lines
        for(int i = 0; i < getPriorityLevels(pp); i++){
        	if(i == 0) g.drawLine(- 1, (PP_DISTANCE_Y * i), PP_DISTANCE_X * (pp.size()-1) + 1, (PP_DISTANCE_Y * i), TpX.FULL_LINE);
        	else g.drawLine(- 1, (PP_DISTANCE_Y * i), PP_DISTANCE_X * (pp.size()-1) + 1, (PP_DISTANCE_Y * i), TpX.DOTTED_LINE);
        }
        
        //Draw the vertical lines
        for(int i = 0; i < pp.size(); i++){
        	if(i == 0) g.drawLine(i * PP_DISTANCE_X, -1, i * PP_DISTANCE_X, 2 + ((getPriorityLevels(pp) - 1) * PP_DISTANCE_Y), TpX.FULL_LINE);
        	else g.drawLine(i * PP_DISTANCE_X, -1, i * PP_DISTANCE_X, 2 + ((getPriorityLevels(pp) - 1) * PP_DISTANCE_Y), TpX.DOTTED_LINE);
        }
        
        //Draw the points
        
        int maxPoints = getPriorityLevels(pp);
        
        for(int i = 0; i < pp.size(); i++){
        	
        	int x = PP_DISTANCE_X * i;	//x coordinate
        	int y =  ((pp.get(i) - getMin(pp))) * PP_DISTANCE_Y;	//y coordinate
        	
        	g.fillOval(x, y, 1, 1, "black", "black");
        }
        
        int yFontDistanceLeft = -10;
        
        //Label the y axis
        g.drawString("$\\pi_{Sched}$", yFontDistanceLeft, (getPriorityLevels(pp) - 1) * PP_DISTANCE_Y, TpX.ALLIGN_LEFT, 2);
        
        for(int i = 0; i < (getPriorityLevels(pp) - 1); i++){
        	g.drawString("$\\pi_{" + Integer.toString(getMin(pp) + i) + "}$", yFontDistanceLeft, (PP_DISTANCE_Y * (i)), TpX.ALLIGN_LEFT, 2);
        }
        
        //Label the x axis
        g.drawString("$Start$", -3.5, -3, TpX.ALLIGN_LEFT, 2);
        g.drawString("$Stop$", (PP_DISTANCE_X * (pp.size()-1)) - 3.5, -3, TpX.ALLIGN_LEFT, 2);
        
        for(int i = 0; i < pp.size() - 2; i++){
        	g.drawString(Integer.toString(i + 1), -0.5 + (PP_DISTANCE_X * (i + 1)), -3, TpX.ALLIGN_LEFT, 2);
        }
	}
	
    /**
     * Function to get the max priority level of this tasks preemption points
     * @param pp
     * @return
     */
    private int getMax(ArrayList<Integer> pp){
    	int max = 0;
    	
    	for (Integer value : pp) {
			if(value > max) max = value;
		}
    	
    	return max;
    }
    
    /**
     * Function to get the min priority level of this preemption point (to make sure they are all larger or equal to the priority)
     * @param pp
     * @return
     */
    private int getMin(ArrayList<Integer> pp){
    	int min = getMax(pp);
    	
    	for (Integer value : pp) {
			if(value < min) min = value;
		}
    	return min;
    }
    
    /**
     * Get the number of used priority levels (max-min)
     * @param pp
     * @return
     */
    private int getPriorityLevels(ArrayList<Integer> pp){
    	int levels = getMax(pp) - getMin(pp);
    	
    	if(getMin(pp) < pp.get(0)) System.err.println("Preemption point less than priority!");
    	
    	return levels + 2;
    }
    
    private void drawResources(TpX g, ArrayList<Integer> preemptionPoints, FPDS_3 fpts3){
    	
		
    	int maxPoints = getPriorityLevels(preemptionPoints);
    	
        Dimension size = getSize();
        Composite origComposite;
    	
		int plan[][][] = fpts3.getResourceLockings();
		int unlockPoint = 0;
		int pointID = 0;
		
		int lockedDistance = 0;
		
		for(int point = 0; point < preemptionPoints.size(); point++){	//draw for all points
			for(int action = 0; action < (getPriorityLevels(preemptionPoints) * 2); action++){
				
				if(plan[point][action][1] == RESOURCE_NOT_USED) break;
				else if(plan[point][action][1] == RESOURCE_LOCKED){
					unlockPoint = searchUnlock(plan, point, preemptionPoints, plan[point][action][0]);
					pointID = plan[point][action][0];
					
					int unlockDistance = getUnlockDistance(pointID, plan, unlockPoint, preemptionPoints);
					
					//Draw the rectangle visualizing the locked area
					g.fillRect((point * PP_DISTANCE_X) + (lockedDistance * PP_LOCKED_DISTANCE) + PP_LOCKED_DISTANCE, 
							0, 
								PP_DISTANCE_X * (unlockPoint - point) - (lockedDistance * PP_LOCKED_DISTANCE) - (unlockDistance * PP_LOCKED_DISTANCE) - (2 * PP_LOCKED_DISTANCE), 
								(pointID - getMin(preemptionPoints)) * PP_DISTANCE_Y, "silver", "silver");
					lockedDistance++;
				}
			}
			lockedDistance = 0;
		}
    }
    
    
    private int getUnlockDistance(int id, int[][][] plan, int unlockPoint, ArrayList<Integer> pp){
    	
    	int retval = 0;
    	
    	for(int i = 0; i < getPriorityLevels(pp); i++){
    		if(plan[unlockPoint][i][1] == RESOURCE_UNLOCKED){
    			if(plan[unlockPoint][i][0] < id) retval++;
    		}
    	}
    	
    	
    	
    	return retval; 
    }
    
//    private int getDistance(ArrayList<Integer> pp, int id){
//    	for (int i = 0; i < pp.size(); i++) {
//			if(pp.get(i) == id){
//				System.out.println("Distance: " + i);
//				return i;
//			}
//		}
//    	return 0;
//    }
    
    private int searchUnlock(int[][][] outPlan, int lockingPP, ArrayList<Integer> pp, int id){
    	int unlockingPP = 0;
    	
    	for(int i = lockingPP+1; i < pp.size(); i++){
    		unlockingPP = i;	//in the worst case it's the last one
    		for(int k = 0; k < (getPriorityLevels(pp) * 2); k++){
    			if(outPlan[i][k][0] == id && outPlan[i][k][1] == -1){
    				return unlockingPP;
    			}
    		}
    	}
    	
    	return unlockingPP;
    }

    /**
     * This method is called from outside to repaint everything with a new FPDS dot algorithm
     * @param fptsVersion
     */
 /*   public void update(int fptsVersion){
    	this.fptsVersion = fptsVersion;
    	Resource.resetCounter();
    	repaint();
    }*/
    
    /**
     * This method draws the textbox with information about the used algorithm, max lockings etc.
     * @param g
     * @param fpts3
     */
//    private void drawTextbox(TpX g, FPTS_3 fpts3){
////		Graphics2D g2 = (Graphics2D) g;
//        Dimension size = getSize();
//        Composite origComposite;
//        
////        Font font = new Font("sansserif", Font.PLAIN, 18);
////        Font fontSmall = new Font("sansserif", Font.BOLD, 12);
////        g.setFont(font);
//        
////        g.setColor(Color.WHITE);
////        g2.fillRect((width - tbWidth), 0, tbWidth,basicHeight);
////        g.setColor(Color.LIGHT_GRAY);
//        g.drawLine((width - tbWidth), 0, (width - tbWidth), basicHeight, TpX.FULL_LINE);
////        g.setColor(Color.GRAY);
//        g.drawLine((width - tbWidth)-1, 0, (width - tbWidth)-1, basicHeight, TpX.FULL_LINE);
//        
//        int textSize = 2;
////        g.setColor(Color.BLACK);
//        g.drawString("Algorithm: ", (int)(size.getWidth() - tbWidth) + 15, 30, TpX.ALLIGN_LEFT, textSize);
//        g.drawString(Integer.toString(fptsVersion), (int)(size.getWidth() - tbWidth) + 125, 30, TpX.ALLIGN_LEFT, textSize);
//        g.drawString("Resources:", (int)(size.getWidth() - tbWidth) + 15, 30 + (1 * 25), TpX.ALLIGN_LEFT, textSize);
//        g.drawString(Integer.toString(fpts3.getNumberOfResources()), (int)(size.getWidth() - tbWidth) + 125, 30 + (1 * 25), TpX.ALLIGN_LEFT, textSize);
//        g.drawString("Lockings:", (int)(size.getWidth() - tbWidth) + 15, 30 + (2 * 25), TpX.ALLIGN_LEFT, textSize);
//        g.drawString(Integer.toString(fpts3.getNumberOfLockings()), (int)(size.getWidth() - tbWidth) + 125, 30 + (2 * 25), TpX.ALLIGN_LEFT, textSize);
//        g.drawString("Unockings:", (int)(size.getWidth() - tbWidth) + 15, 30 + (3 * 25), TpX.ALLIGN_LEFT, textSize);
//        g.drawString(Integer.toString(fpts3.getNumberOfUnlockings()), (int)(size.getWidth() - tbWidth) + 125, 30 + (3 * 25), TpX.ALLIGN_LEFT, textSize);
//    }
    
    /**
     * This method is used to print all calls to the AUTOSAR/OSEK primitives in a table below the graph
     * @param g	Graphics object we use to draw on the panel
     * @param fpts3	The instance of the FPDS dot algorithm we use
     */
    private void drawPrimitiveCalls(TpX g, FPDS_3 fpts3, ArrayList<Integer> pp){
    	
    	int[][][] calls = fpts3.getResourceLockings();	//returns the list with all lockings
    	int maxCalls = getMaxNumberOfLockingsPerPoint(fpts3, pp);

    	int distanceToGraph = 5;
		int rowHeight = 3;
		height = basicHeight + (maxCalls * rowHeight);  
        
        //draw the vertical lines
        for(int i = 0; i < (pp.size()); i++){	//we draw one line between each PP
        	g.drawLine(-(PP_DISTANCE_X / 2) + (PP_DISTANCE_X * i), -distanceToGraph, -(PP_DISTANCE_X / 2) + (PP_DISTANCE_X * i), -distanceToGraph - ((maxCalls) * rowHeight), TpX.FULL_LINE);
        }
        
        //draw the horizontal lines 
        for(int i = 0; i < maxCalls - 1; i++){
        	g.drawLine(-(PP_DISTANCE_X / 2)-3, -distanceToGraph - ((i + 1) * rowHeight), ((pp.size() - 1) * PP_DISTANCE_X) + (PP_DISTANCE_X / 2), -distanceToGraph - ((i + 1) * rowHeight), TpX.FULL_LINE);
        	//g.drawString(Integer.toString(i + 1), -(PP_DISTANCE_X / 2)-2, (-(((i + 1) * rowHeight))- distanceToGraph +1), TpX.ALLIGN_LEFT, 2);
        }
        
        for(int i = 0; i < maxCalls; i++){
        	g.drawString(Integer.toString(i + 1), -(PP_DISTANCE_X / 2)-2, (-(((i + 1) * rowHeight))- distanceToGraph +1), TpX.ALLIGN_LEFT, 2);
        }
        
        //add the text to the cells
        String label = null;
        
        for(int point = 0; point < pp.size(); point++){
        	for(int call = 0; call < maxCalls; call++){
        		if(calls[point][call][1] == 0) break;	//stop if there is no locking command in this cell!
        		int resource = calls[point][call][0];
        		
        		//get the command and build the string
        		if(calls[point][call][1] == 1){	//call to lock the resource
        			label = new String("Get(" + resource + ")");
        			if(resource == getMax(pp) + 1) label = "Get(SCHED)";
        		}else if(calls[point][call][1] == -1){	//call to unlock the resource
        			label = new String("Rel(" + resource + ")");
        			if(resource == getMax(pp) + 1) label = "Rel(SCHED)";
        		}
        		g.drawString(label, -(PP_DISTANCE_X / 2) + (PP_DISTANCE_X * point) + 1, -((call + 1) * rowHeight) - distanceToGraph + 0.75, TpX.ALLIGN_LEFT, 2);
        	}
        }
    }
    	
    /**
     * This function returns the maximum number of calls to AUTOSAR/OSEK primitives of one preemption point
     * @param fpts3 The instance of the FPDS dot algorithm we use
     * @param pp	The preemption point configuration of the task
     * @return		Maximum number of calls to the AUTOSAR/OSEK primitive of any of the preemption points
     */
    private int getMaxNumberOfLockingsPerPoint(FPDS_3 fpts3, ArrayList<Integer> pp){
    	int retval = 0;
    	
    	int[][][] calls = fpts3.getResourceLockings();	//array with all information about locking and unlocking of resources
    	int numberOfPP = pp.size();						//number of preemption points
    	int tmpCount = 0;
    	
    	for(int i = 0; i < numberOfPP; i++){
    		for(int k = 0; k < (fpts3.getNumberOfResources() * 2); k++){
    			if(calls[i][k][1] == 0) break;	//check if this entry is not used
    			tmpCount++;
    		}
    		if(tmpCount > retval) retval = tmpCount;
    		tmpCount = 0;
    	}
    	
    	return retval;
    }
    
    /**
     * Returns the dimensions of the canvas
     * @return
     */
    private Dimension getSize(){
    	Dimension d = new Dimension();
    	
    	d.height = height;
    	d.width = width;
    	return d;
    }
    
    /**
     * Used so we can change the algorithm version from outside
     * @param version
     */
    public void setAlgorithmVersion(int version){
    	fptsVersion = version;
    }
}
