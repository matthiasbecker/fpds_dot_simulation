import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class JSONReader {
	
	public JSONReader(){

	}
	
	public static ArrayList<Integer> getModPreemptionPoints(File file){
		
		int priority = 0;
		ArrayList<Integer>preemptionPoint = null;
		JSONParser parser = new JSONParser();
		
		try{
			Object obj = parser.parse(new FileReader(file));
			 
			JSONObject jsonObject = (JSONObject) obj;
			
			double prio = (Double)jsonObject.get("Priority");
			priority = (int) prio;
			//System.out.println("Task Priority: " + priority);
			
			JSONArray preemptionPoints = (JSONArray) jsonObject.get("PreemptionPoints");
			
			Iterator<JSONObject> iterator = preemptionPoints.iterator();
			int index = 0;
			preemptionPoint = new ArrayList<Integer>();
			preemptionPoint.add(priority);	//the first PP is the task priority
			while(iterator.hasNext()){
				JSONObject tmp = iterator.next();				
				
				double pp = (Double)tmp.get("pp");
			//	System.out.println("PreemptionPoint " + index + " : " + (int)pp);
				preemptionPoint.add((int)pp);
				index++;
			}
			preemptionPoint.add(priority);	//the last PP is also the task priority
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
		
		return preemptionPoint;
	}
	
}
