package TpX;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class RecordWriter {
	private  String fileName = null;
	private  FileWriter fileWriter = null;
	private  BufferedWriter outputBuffer = null;
	
	public void InitializeRecord(String name)
	{
		
		File f = new File(name);
		if(f.exists() && !f.isDirectory()) { 
			f.delete();
		}
		
		try
		{
			fileName = name;
			fileWriter = new FileWriter(fileName, true);
		}
		catch (Exception ex){}
		
		outputBuffer = new BufferedWriter(fileWriter);
	}
	
	/**
	 * This method finalizes the trace and writes everything to the file
	 */
	public void CloseRecord()
	{
		try
		{
			outputBuffer.close();
			fileWriter.close();		
			fileWriter = null;
		}
		catch(Exception e) {}
	}
	
	/**
	 * This method is used to write a new line to the file
	 * @param text
	 */
	public void WriteToFile(String text)
	{
		if(fileWriter == null){
			InitializeRecord("record.csv");
		}
		try
		{
			outputBuffer.write(text + "\r\n");
		}
		catch(Exception e)
		{
			System.err.println("RecordWriter, something went wrong...");
		}	
	}

}
