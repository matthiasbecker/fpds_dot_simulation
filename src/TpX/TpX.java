package TpX;

public class TpX {
	
	private String filename = null;
	private RecordWriter writer = null;
	
	public final static int FULL_LINE	= 0;
	public final static int DASHED_LINE	= 1;
	public final static int DOTTED_LINE	= 2;
	public final static char ALLIGN_CENTER = 'c';
	public final static char ALLIGN_LEFT = 'l';
	public final static char ALLIGN_RIGHT = 'r';
	
	/**
	 * Constructor to initialize everything so we can start drawing.
	 * @param filename
	 */
	public TpX(String filename, String caption, String label) {
		this.filename = filename;
		writer = new RecordWriter();
		writer.InitializeRecord(filename + ".TpX");
		
		writer.WriteToFile("%<TpX v=\"5\" ArrowsSize=\"0.7\" StarsSize=\"1\" DefaultFontHeight=\"5\" DefaultSymbolSize=\"30\" ApproximationPrecision=\"0.01\" PicScale=\"1.0\" Border=\"0\" BitmapRes=\"20000\" HatchingStep=\"2\" DottedSize=\"0.5\" DashSize=\"1\" LineWidth=\"0.2\" IncludePath=\"figures/\">");
		writer.WriteToFile("%  <caption label=\"" + label + "\">");
		writer.WriteToFile("%" + caption + "  </caption>");
	}
	
	/**
	 * Draw one line 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param linetype
	 */
	public void drawLine(int x1, int y1, int x2, int y2, int linetype){
		if(writer != null){
			String argument = "%  <line x1=\""+ x1 +"\" y1=\""+ y1 +"\" x2=\""+ x2 +"\" y2=\"" + y2 + "\"";
			if(linetype != FULL_LINE){
				if(linetype == DASHED_LINE) argument +=  " li=\"dot\" lw=\"0.50\"";
				else if(linetype == DOTTED_LINE) argument +=  " li=\"dot\" lw=\"0.50\"";
			}
			argument+= "/>";
			writer.WriteToFile(argument);
		}else{
			System.err.println("No writer...");
		}
	}
	
	/**
	 * Draw a rectangle
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param fillColor
	 */
	public void fillRect(int x, int y, int width, int height, String fillColor, String lineColor){
		if(writer != null){
			//%  <rect x="51.4617" y="70.9195" w="12.4937" h="3.16296" li="none" fill="white"/>
			String argument = null;
			argument = "%  <rect x=\"" + x + "\" y=\"" + y + "\" w=\"" + width + "\" h=\"" + height + "\" lc=\"" + lineColor + "\" fill=\"" + fillColor + "\"/>";
			
			writer.WriteToFile(argument);
		}else{
			System.err.println("No writer...");
		}
	}
	
	/**
	 * Draw text on the canvas
	 * @param x
	 * @param y
	 * @param text
	 * @param allignment
	 * @param size
	 */
	public void drawString(String text, double x, double y, char allignment, double size){
		if(writer != null){
			//%  <text x="57.5" y="76.5" t="$R_6$" tex="$R_6$" h="3" halign="c"/>
			String argument = null;
			argument = "%  <text x=\"" + x + "\" y=\"" + y + "\" t=\"" + text + "\" tex=\"" + text + "\" h=\"" + size + "\" halign=\"" + allignment + "\"/>";
			
			writer.WriteToFile(argument);
		}else{
			System.err.println("No writer...");
		}
	}
	
	/**
	 * Draw an ellipse on the canvas
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param fillColor
	 */
	public void fillOval(int x, int y, int width, int height, String fillColor, String lineColor){
		if(writer != null){
			//%  <ellipse x="53.6489" y="107.461" dx="34.1805" dy="2.92137" rotdeg="0.000524346" lw="0.50" lc="springgreen"/>
			String argument = null;
			argument = "%  <ellipse x=\"" + x + "\" y=\"" + y + "\" dx=\"" + width + "\" dy=\"" + height + "\" lc=\"" + lineColor + "\" fill=\"" + fillColor + "\"/>";
			
			writer.WriteToFile(argument);
		}else{
			System.err.println("No writer...");
		}
	}
	
	/**
	 * Finalizes the file and closes the writer
	 */
	public void closeFile(){
		writer.WriteToFile("%</TpX>");
		writer.CloseRecord();
		writer = null;
	}
}
