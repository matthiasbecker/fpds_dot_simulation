import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import javax.swing.JApplet;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import FPDS_3.FPDS_3;


public class main extends JApplet implements KeyListener{

	private static FPDSGraph painter;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

//	private static final int PP_DISTANCE_X = 80;
//	private static final int PP_DISTANCE_Y = 50;
//	private static final int BORDER_LEFT = 70;
//	private static final int TEXTBOX_WIDTH = 160;
	
	private static File taskConfigFile = null;
	private static JFrame f = null;
	private static ArrayList<Integer> preemptionPoints = null;
	private static FPDS_PDF pdfGen = null;
	
	public static void main(String[] args) {
		System.out.println("FPDS\u25cf in AUTOSAR");
		
		//JOptionPane t = new JOptionPane();
		//JOptionPane.showInputDialog("test", "test2");
		//JOptionPane.showInputDialog("Test");
		//JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), "test");
		JFileChooser fc = new JFileChooser();
		if(Config.getInst().getChooseFile()) fc.showDialog(JOptionPane.getRootFrame(), "Select task description file");
		taskConfigFile = null;
		if(fc.getSelectedFile()  != null){
			taskConfigFile = fc.getSelectedFile();
		}else{
			taskConfigFile = new File(Config.getInst().getDefaultPath());
		}
		
		pdfGen = new FPDS_PDF(taskConfigFile);
		
		preemptionPoints = JSONReader.getModPreemptionPoints(taskConfigFile); //get the list with preemption points 
		int levels = getPriorityLevels(preemptionPoints) + 2; //we need to add the scheduler
		int points = preemptionPoints.size();
		
		f = new JFrame("FPDS \u25cf in AUTOSAR");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
        JApplet ap = new main();
        
        ap.init();
        ap.start();
        f.add("Center", ap);
        //f.pack();
       // f.setSize(10, 10);
        //f.setSize((points - 1) * PP_DISTANCE_X + BORDER_LEFT + 50 + TEXTBOX_WIDTH, levels * PP_DISTANCE_Y + 50);//125 + 100 * (pp.size()-1)
        f.setVisible(true);
        f.setResizable(false);
	}
	
    public void init() {
        /* Turn off metal's use of bold fonts */
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        setFocusable( true );	//windows somehow needs this line to make the keylistener work (on OSX it worked fine without it)
    }
	
    public void start() {
    	//setLayout(new BorderLayout());
    	painter = new FPDSGraph(taskConfigFile);
    	
    	JPanel pannel = new JPanel();
    	
    	//setLayout(new BorderLayout());p.add(new JLabel("Temperature:"));
        add("Center", painter);
        addKeyListener(this);
        this.update(pannel.getGraphics());
    }

    /**
     * Function to get the max priority level of this tasks preemption points
     * @param pp
     * @return
     */
    private static int getMax(ArrayList<Integer> pp){
    	int max = 0;
    	
    	for (Integer value : pp) {
			if(value > max) max = value;
		}
    	
    	return max;
    }
    
    /**
     * Function to get the min priority level of this preemption point (to make sure they are all larger or equal to the priority)
     * @param pp
     * @return
     */
    private static int getMin(ArrayList<Integer> pp){
    	int min = getMax(pp);
    	
    	for (Integer value : pp) {
			if(value < min) min = value;
		}
    	return min;
    }
    
    /**
     * Get the number of used priority levels (max-min)
     * @param pp
     * @return
     */
    private static int getPriorityLevels(ArrayList<Integer> pp){
    	int levels = getMax(pp) - getMin(pp);
    	
    	if(getMin(pp) < pp.get(0)) System.err.println("Preemption point less than priority!");
    	
    	return levels;
    }

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		//System.out.println("Key pressed: " + arg0.getKeyChar());
		if(arg0.getKeyChar() == '1'){
			painter.update(1);
			pdfGen.setAlgorithmVersion(1);
		}
		else if(arg0.getKeyChar() == '2'){
			painter.update(2);
			pdfGen.setAlgorithmVersion(2);
		}
		else if(arg0.getKeyChar() == '3'){
			painter.update(3);
			pdfGen.setAlgorithmVersion(3);
		}
		else if(arg0.getKeyChar() == '4'){
			painter.update(4);
			pdfGen.setAlgorithmVersion(4);
		}
		else if(arg0.getKeyChar() == 'p' || arg0.getKeyChar() == 'P'){
			
			//JOptionPane.showMessageDialog(null, "Printed Graph", "Info",JOptionPane.PLAIN_MESSAGE);
			String name = JOptionPane.showInputDialog(null, "File name");
			if(name != null){
				pdfGen.generatePDF(name);
			}
		}
		else{
			JOptionPane.showMessageDialog(null, "Invalid Command", "ERROR",JOptionPane.ERROR_MESSAGE);
			
			//int levels = getPriorityLevels(preemptionPoints) + 2; //we need to add the scheduler
			//int points = preemptionPoints.size();
	        //f.setSize((points - 1) * PP_DISTANCE_X + BORDER_LEFT + 50 + TEXTBOX_WIDTH, levels * PP_DISTANCE_Y + 50 + 100);//125 + 100 * (pp.size()-1)
		}
	}
    
}
